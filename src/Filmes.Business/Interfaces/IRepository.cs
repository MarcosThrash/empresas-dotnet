﻿using Filmes.Business.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Filmes.Business.Interfaces
{
    public interface IRepository<TEntity> : IDisposable where TEntity : Entity
    {

        Task Adicionar(TEntity entity);

        Task<TEntity> ObterPorId(Guid id);

        Task<List<TEntity>> ObterTodos();

        Task<int> SaveChanges();

    }
}
