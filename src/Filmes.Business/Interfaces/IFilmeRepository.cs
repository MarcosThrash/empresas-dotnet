﻿using Filmes.Business.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Filmes.Business.Interfaces
{
    public interface IFilmeRepository : IRepository<Filme>
    {       
                        
        Task<Filme> ObterFilmeDetalhado(Guid id);

        Task<List<Filme>> ObterFilmesDetalhados();


    }
}
