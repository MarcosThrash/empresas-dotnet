﻿using Filmes.Business.Models;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Filmes.Business.Validations
{
    public class FilmeValidacao: AbstractValidator<Filme>
    {
        public static string NomeErroMensagem => "Nome não pode ser vázio";
        public static string SinopseErroMensagem => "Nome não pode ser vázio";
        public static string GeneroErroMensagem => "Genero não pode ser vázio";
        public static string DiretorErroMensagem => "Diretor não pode ser vázio";


        public FilmeValidacao()
        {
            RuleFor(f => f.Nome)
                .NotEmpty()
                .WithMessage(NomeErroMensagem);

            RuleFor(f => f.Sinopse)
                .NotEmpty()
                .WithMessage(SinopseErroMensagem);

            RuleFor(f => f.DiretorId)
                .NotEmpty()
                .WithMessage(DiretorErroMensagem);

            RuleFor(f => f.GeneroId)
                .NotEmpty()
                .WithMessage(GeneroErroMensagem);

        }

    }
}
