﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Filmes.Business.Models
{
    public abstract class Entity
    {
        public Guid Id { get; set; }

        public Entity()
        {
            Id = new Guid();
        }
    }
}
