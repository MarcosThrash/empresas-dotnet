﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Filmes.Business.Models
{
    public class FilmeAtor : Entity
    {

        public Guid FilmeId { get; set; }

        public Guid AtorId { get; set; }

        public Filme Filme { get; set; }

        public Ator Ator { get; set; }

    }
}
