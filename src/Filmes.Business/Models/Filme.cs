﻿using Filmes.Business.Validations;
using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Filmes.Business.Models
{
    public class Filme : Entity
    {

        public string Nome { get; set; }

        public string Sinopse { get; set; }

        public Guid GeneroId { get; set; }

        public Guid DiretorId { get; set; }

        public Genero Genero { get; set; }

        public Diretor Diretor { get; set; }

        public IEnumerable<FilmeAtor> FilmeAtores { get; set; }

        public IEnumerable<Voto> Votos { get; set; }



        public ValidationResult Valido()
        {
            return new FilmeValidacao().Validate(this);
        }

        public int NumeroDeVotos()
        {
            return Votos.Count();
        }

        public double MediaVotos()
        {
            return (Votos.Count() > 0 ? Votos.Select(v => v.Nota).Average() : 0);
        }

    }
}
