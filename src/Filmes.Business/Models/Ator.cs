﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Filmes.Business.Models
    {
    public class Ator : Entity
    {

        public string Nome { get; set; }

        public IEnumerable<FilmeAtor> FilmesAtor { get; set; }

    }
}
