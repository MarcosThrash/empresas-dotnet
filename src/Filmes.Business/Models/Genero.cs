﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Filmes.Business.Models
{
    public class Genero : Entity
    {

        public string Nome { get; set; }

        public IEnumerable<Filme> Filmes { get; set; }

    }

}
