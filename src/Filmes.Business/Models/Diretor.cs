﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Filmes.Business.Models
{
    public class Diretor : Entity
    {
        public string Nome { get; set; }

        public IEnumerable<Filme> Filmes { get; set; }

    }
}
