﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Filmes.Business.Models
{
    public class Voto : Entity
    {

        public double Nota { get; set; }

        public Guid FilmeId { get; set; }

        public Guid? UsuarioId { get; set; }

        public Filme Filme { get; set; }

    }
}
