﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Filmes.Api.DTOs
{
    public class AtorDTO
    {

        public Guid Id { get; set; }

        public string Nome { get; set; }

        public IEnumerable<FilmeAtorDTO> FilmesAtorDTO { get; set; }

    }
}
