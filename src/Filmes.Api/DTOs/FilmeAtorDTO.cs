﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Filmes.Api.DTOs
{
    public class FilmeAtorDTO
    {

        public Guid Id { get; set; }

        public Guid FilmeDTOId { get; set; }

        public Guid AtorDTOId { get; set; }

        public FilmeDTO FilmeDTO { get; set; }

        public AtorDTO AtorDTO { get; set; }

    }
}
