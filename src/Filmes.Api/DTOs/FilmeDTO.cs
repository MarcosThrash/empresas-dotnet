﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Filmes.Api.DTOs
{
    public class FilmeDTO
    {
        [Key]
        public Guid Id { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        [StringLength(100, ErrorMessage = "O campo {0} precisa ter entre  e 100 caracteres", MinimumLength = 2)]
        public string Nome { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        [StringLength(700, ErrorMessage = "O  campo {0} precisa ter entre {1} e {2} caracteres", MinimumLength = 2)]
        public string Sinopse { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        public Guid GeneroId { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        public Guid DiretorId { get; set; }
      
        public IEnumerable<string> Atores { get; set; }

    }
}
