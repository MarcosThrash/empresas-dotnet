﻿using System;
using System.Collections.Generic;

namespace Filmes.Api.DTOs
{
    public class FilmeApresentacaoDTO
    {

        public FilmeApresentacaoDTO()
        {
            Atores = new List<string>();
        }

        public Guid Id { get; set; }

        public string Nome { get; set; }

        public string Sinopse { get; set; }
      
        public string Genero { get; set; }

        public string Diretor { get; set; }

        public int NumeroDeVotos { get; set; }

        public double MediaVotos { get; set; }

        public IEnumerable<string> Atores { get; set; }

    }
}
