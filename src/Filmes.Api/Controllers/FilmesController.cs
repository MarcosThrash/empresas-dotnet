﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Filmes.Api.DTOs;
using Filmes.Business.Interfaces;
using Filmes.Business.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Filmes.Api.Controllers
{
    [Route("api/filmes")]    
    public class FilmesController : MainController
    {

        private readonly IFilmeRepository _filmeRepository;
        private readonly IMapper _mapper;

        public FilmesController(IFilmeRepository filmeRepository, IMapper mapper)
        {
            _filmeRepository = filmeRepository;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<FilmeApresentacaoDTO>>> ListarTodos()
        {
            try
            {
                var lista = _mapper.Map<IEnumerable<FilmeApresentacaoDTO>>(await _filmeRepository.ObterFilmesDetalhados());
                return Ok(lista);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Erro no Servidor");
            }
            
        }

        [HttpGet("{id:guid}")]
        public async Task<ActionResult<IEnumerable<FilmeApresentacaoDTO>>> ObterPorId(Guid id)
        {
            try
            {
                var filme = await ObterFilmePorId(id);
                if (filme == null) return NotFound();
                return Ok(filme);
            }
            catch (Exception e)
            {
                return StatusCode(500, "Erro no Servidor");
            }
            
        }

        [HttpPost]
        public async Task<ActionResult<FilmeDTO>> CadastrarFilme(FilmeDTO filmeDTO)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest();
                await _filmeRepository.Adicionar(_mapper.Map<Filme>(filmeDTO));
                return Ok(filmeDTO);
            }
            catch (Exception)
            {
                return StatusCode(500, "Erro no Servidor");
            }
            
        }


        public async Task<FilmeApresentacaoDTO> ObterFilmePorId(Guid id)
        {
            var filme = await _filmeRepository.ObterFilmeDetalhado(id);
            return _mapper.Map<FilmeApresentacaoDTO>(await _filmeRepository.ObterFilmeDetalhado(id));
        }

    }
}
