﻿using AutoMapper;
using Filmes.Api.DTOs;
using Filmes.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Filmes.Api.Config
{
    public class AutomapperConfig : Profile
    {

        public AutomapperConfig()
        {
            CreateMap<Filme, FilmeDTO>();
            CreateMap<FilmeDTO, Filme>();

            CreateMap<Filme, FilmeApresentacaoDTO>()
                .ForMember(dest => dest.Diretor, opt => opt.MapFrom(src => src.Diretor.Nome))
                .ForMember(dest => dest.Atores, opt => opt.MapFrom(src => src.FilmeAtores.Select(f => f.Ator.Nome)))
                .ForMember(dest => dest.Genero, opt => opt.MapFrom(src => src.Genero.Nome))
                .ForMember(dest => dest.NumeroDeVotos, opt => opt.MapFrom(src => src.NumeroDeVotos()))
                .ForMember(dest => dest.MediaVotos, opt => opt.MapFrom(src => Math.Round(src.MediaVotos(), 1)));
        }


    }
}
