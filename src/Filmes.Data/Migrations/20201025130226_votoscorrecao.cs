﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Filmes.Data.Migrations
{
    public partial class votoscorrecao : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Notas_Filmes_FilmeId",
                table: "Notas");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Notas",
                table: "Notas");

            migrationBuilder.RenameTable(
                name: "Notas",
                newName: "Votos");

            migrationBuilder.RenameIndex(
                name: "IX_Notas_FilmeId",
                table: "Votos",
                newName: "IX_Votos_FilmeId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Votos",
                table: "Votos",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Votos_Filmes_FilmeId",
                table: "Votos",
                column: "FilmeId",
                principalTable: "Filmes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Votos_Filmes_FilmeId",
                table: "Votos");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Votos",
                table: "Votos");

            migrationBuilder.RenameTable(
                name: "Votos",
                newName: "Notas");

            migrationBuilder.RenameIndex(
                name: "IX_Votos_FilmeId",
                table: "Notas",
                newName: "IX_Notas_FilmeId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Notas",
                table: "Notas",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Notas_Filmes_FilmeId",
                table: "Notas",
                column: "FilmeId",
                principalTable: "Filmes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
