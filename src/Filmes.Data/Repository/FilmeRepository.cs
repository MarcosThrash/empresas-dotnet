﻿using Filmes.Business.Interfaces;
using Filmes.Business.Models;
using Filmes.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filmes.Data.Repository
{
    public class FilmeRepository : Repository<Filme>, IFilmeRepository
    {

        public FilmeRepository(ContextFilmes _context):base(_context)
        {
            
        }

        public async Task<Filme> ObterFilmeDetalhado(Guid id)
        {
            return await _context.Filmes.AsNoTracking()
                .Include(f => f.Genero)
                .Include(f => f.FilmeAtores).ThenInclude(a => a.Ator)
                .Include(f => f.Diretor)
                .Include(f => f.Votos)
                .FirstOrDefaultAsync(f => f.Id == id);
        }

        public async Task<List<Filme>> ObterFilmesDetalhados()
        {
            return await _context.Filmes.AsNoTracking()
                .Include(f => f.Genero)
                .Include(f => f.FilmeAtores).ThenInclude(a => a.Ator)
                .Include(f => f.Votos)
                .Include(f => f.Diretor)
                .ToListAsync();
        }

       
    }
}
