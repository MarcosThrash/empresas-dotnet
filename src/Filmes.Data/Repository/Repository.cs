﻿using Filmes.Business.Interfaces;
using Filmes.Business.Models;
using Filmes.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Filmes.Data.Repository
{
    public abstract class Repository<TEntity> : IRepository<TEntity> where TEntity : Entity, new()
    {

        protected readonly ContextFilmes _context;

        protected readonly DbSet<TEntity> _dbset;

        public Repository(ContextFilmes context)
        {
            _context = context;
            _dbset = context.Set<TEntity>();

        }


        public virtual async Task<List<TEntity>> ObterTodos()
        {
            return await _dbset.ToListAsync();
        }

        public virtual async Task<TEntity> ObterPorId(Guid id)
        {
            return await _dbset.FindAsync(id);
        }
       
        public virtual async Task Adicionar(TEntity entity)
        {
            _dbset.Add(entity);
            await SaveChanges();
        }
       
       
        public async Task<int> SaveChanges()
        {
            return await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context?.Dispose();
        }

    }
}
