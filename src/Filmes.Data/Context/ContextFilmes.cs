﻿using Filmes.Business.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Filmes.Data.Context
{
    public class ContextFilmes : DbContext
    {
        public ContextFilmes(DbContextOptions options) : base(options)
        {  
        
        }

        public DbSet<Filme> Filmes { get; set; }
        public DbSet<Ator> Atores { get; set; }
        public DbSet<Genero> Generos { get; set; }
        public DbSet<FilmeAtor> FilmesAtores { get; set; }
        public DbSet<Diretor> Diretores { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(ContextFilmes).Assembly);
            base.OnModelCreating(modelBuilder);
        }

    }
}
