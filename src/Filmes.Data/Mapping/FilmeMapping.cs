﻿using Filmes.Business.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Filmes.Data.Mapping
{
    public class FilmeMapping : IEntityTypeConfiguration<Filme>
    {
        public void Configure(EntityTypeBuilder<Filme> builder)
        {
            builder.HasKey(f => f.Id);

            builder.Property(f => f.Nome)
                .IsRequired()
                .HasColumnType("varchar(100)");

            builder.Property(f => f.Sinopse)
               .IsRequired()
               .HasColumnType("varchar(700)");

            builder.HasMany(f => f.FilmeAtores)
                .WithOne(fa => fa.Filme)
                .HasForeignKey(fa => fa.FilmeId);

            builder.HasMany(f => f.Votos)
                .WithOne(v => v.Filme)
                .HasForeignKey(v => v.FilmeId);

            builder.ToTable("Filmes");

        }
    }
}
