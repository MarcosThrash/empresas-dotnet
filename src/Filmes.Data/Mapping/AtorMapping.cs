﻿using Filmes.Business.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Filmes.Data.Mapping
{
    public class AtorMapping : IEntityTypeConfiguration<Ator>
    {
        public void Configure(EntityTypeBuilder<Ator> builder)
        {
            builder.HasKey(d => d.Id);

            builder.Property(d => d.Nome)
                .IsRequired()
                .HasColumnType("varchar(100)");

            builder.HasMany(d => d.FilmesAtor)
                .WithOne(fa => fa.Ator)
                .HasForeignKey(fa => fa.AtorId);

            builder.ToTable("Atores");
        }
    }
}
