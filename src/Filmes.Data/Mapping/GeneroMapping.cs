﻿using Filmes.Business.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Filmes.Data.Mapping
{
    public class GeneroMapping : IEntityTypeConfiguration<Genero>
    {
        public void Configure(EntityTypeBuilder<Genero> builder)
        {
            builder.HasKey(d => d.Id);

            builder.Property(d => d.Nome)
                .IsRequired()
                .HasColumnType("varchar(100)");

            builder.HasMany(d => d.Filmes)
                .WithOne(f => f.Genero)
                .HasForeignKey(f => f.GeneroId);

            builder.ToTable("Generos");

        }
    }
}
