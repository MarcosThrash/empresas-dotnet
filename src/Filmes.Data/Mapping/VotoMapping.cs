﻿using Filmes.Business.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Filmes.Data.Mapping
{
    class VotoMapping : IEntityTypeConfiguration<Voto>
    {
        public void Configure(EntityTypeBuilder<Voto> builder)
        {

            builder.HasKey(v => v.Id);

            builder.Property(v => v.Nota)
                .IsRequired();         

            builder.ToTable("Votos");

        }
    }
}
