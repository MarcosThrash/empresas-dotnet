﻿using Filmes.Business.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Filmes.Data.Mapping
{
    public class FilmeAtorMapping : IEntityTypeConfiguration<FilmeAtor>
    {
        public void Configure(EntityTypeBuilder<FilmeAtor> builder)
        {
            builder.HasKey(fa => fa.Id);

            builder.ToTable("FilmesAtores");

        }
    }
}
