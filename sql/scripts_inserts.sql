
INSERT [dbo].[Atores] ([Id], [Nome]) VALUES (N'9e5318c4-ce24-4594-82c5-2f05ea0276bc', N'Anthony Hopkins')

INSERT [dbo].[Atores] ([Id], [Nome]) VALUES (N'de9a522a-5dac-4eb3-87bc-5e8c0a18f980', N'Winona Ryder')
 
INSERT [dbo].[Atores] ([Id], [Nome]) VALUES (N'a4d9188c-5ebf-471e-912c-7bf7618e4312', N'Robert De Niro')
 
INSERT [dbo].[Atores] ([Id], [Nome]) VALUES (N'ca894cad-266c-45d6-bcca-947f8e223cc5', N'Christopher Lee')
 
INSERT [dbo].[Atores] ([Id], [Nome]) VALUES (N'4a11b945-f9f0-444f-9606-cdfacfa77833', N'Keanu Reeves')
 
INSERT [dbo].[Diretores] ([Id], [Nome]) VALUES (N'9fc9762a-bf4a-443a-a494-3d685aabb727', N'Francis Ford Coppola')
 
INSERT [dbo].[Diretores] ([Id], [Nome]) VALUES (N'61ed84a3-6340-4d6a-b1cf-520a279845aa', N'Peter Jackson')
 
INSERT [dbo].[Diretores] ([Id], [Nome]) VALUES (N'7e33d5a4-cd21-406c-bfa7-56bc1bc2de51', N'William Friedkin')
 
INSERT [dbo].[Generos] ([Id], [Nome]) VALUES (N'4111812d-5bfb-413a-9a23-08e8b5162179', N'Terror')
 
INSERT [dbo].[Generos] ([Id], [Nome]) VALUES (N'4b5170b7-84ba-4726-b0db-e02e13a3627e', N'Fantasia')
 
INSERT [dbo].[Filmes] ([Id], [Nome], [Sinopse], [GeneroId], [DiretorId]) VALUES (N'da5f9c79-5f54-49f7-bc6a-71b2fc295000', N'Senhor dos Anéis', N'A história de O Senhor dos Anéis ocorre num tempo e espaço imaginário, a Terceira Era da Terra Média, que é um mundo inspirado na Terra real, mais especificamente, segundo Tolkien, numa Europa mitológica, habitado por Humanos e por outras raças: Elfos, Anãos (anteriormente "Anões"), Hobbits e Orques (anteriormente "Orcs"). ', N'4b5170b7-84ba-4726-b0db-e02e13a3627e', N'61ed84a3-6340-4d6a-b1cf-520a279845aa')
 
INSERT [dbo].[Filmes] ([Id], [Nome], [Sinopse], [GeneroId], [DiretorId]) VALUES (N'e56815f7-55e9-45d1-99ee-82926fa7c375', N'Drácula de Bram Stoker', N'O filme conta a história do líder romeno Vlad Tepes (Vlad Drácula). Em 1462, Vlad Drácula, membro da Ordem do Dragão, retorna de uma vitória contra os turcos e descobre que sua esposa Elisabeta cometeu suicídio depois que seus inimi s relataram falsamente sua morte.', N'4111812d-5bfb-413a-9a23-08e8b5162179', N'9fc9762a-bf4a-443a-a494-3d685aabb727')
 
INSERT [dbo].[Filmes] ([Id], [Nome], [Sinopse], [GeneroId], [DiretorId]) VALUES (N'3bc43f0c-08b5-43f3-9305-90eb90af7b58', N'O Exorcista', N'Em Georgetown, Washington, uma atriz vai gradativamente tomando consciência que a sua filha de doze anos está tendo um comportamento completamente assustador. Deste modo, ela pede ajuda a um padre, que também um psiquiatra, e este chega a conclusão de que a garota está possuída pelo demônio', N'4111812d-5bfb-413a-9a23-08e8b5162179', N'7e33d5a4-cd21-406c-bfa7-56bc1bc2de51')
 
INSERT [dbo].[FilmesAtores] ([Id], [FilmeId], [AtorId]) VALUES (N'ab596a45-6e43-4520-b0a7-6cd9ff7879b0', N'e56815f7-55e9-45d1-99ee-82926fa7c375', N'de9a522a-5dac-4eb3-87bc-5e8c0a18f980')
 
INSERT [dbo].[FilmesAtores] ([Id], [FilmeId], [AtorId]) VALUES (N'2098c905-ee47-4b4b-a9d5-6d2341c8e18f', N'e56815f7-55e9-45d1-99ee-82926fa7c375', N'9e5318c4-ce24-4594-82c5-2f05ea0276bc')
 
INSERT [dbo].[FilmesAtores] ([Id], [FilmeId], [AtorId]) VALUES (N'7a52171d-df13-4e21-8663-c01418673d05', N'da5f9c79-5f54-49f7-bc6a-71b2fc295000', N'ca894cad-266c-45d6-bcca-947f8e223cc5')
 
INSERT [dbo].[FilmesAtores] ([Id], [FilmeId], [AtorId]) VALUES (N'd382b048-af4b-4249-995e-c52df965925a', N'e56815f7-55e9-45d1-99ee-82926fa7c375', N'4a11b945-f9f0-444f-9606-cdfacfa77833')
 
INSERT [dbo].[Votos] ([Id], [Nota], [FilmeId], [UsuarioId]) VALUES (N'f09deb71-f114-4e06-8bc2-12355e8ba7ae', 4, N'e56815f7-55e9-45d1-99ee-82926fa7c375', NULL)
 
INSERT [dbo].[Votos] ([Id], [Nota], [FilmeId], [UsuarioId]) VALUES (N'ede27bac-e14e-45bf-9116-4dd726ed67d5', 3.8, N'e56815f7-55e9-45d1-99ee-82926fa7c375', NULL)
 
INSERT [dbo].[Votos] ([Id], [Nota], [FilmeId], [UsuarioId]) VALUES (N'0b25894e-e1dc-4962-8526-519db599bfea', 3.5, N'e56815f7-55e9-45d1-99ee-82926fa7c375', NULL)
 
INSERT [dbo].[Votos] ([Id], [Nota], [FilmeId], [UsuarioId]) VALUES (N'1e4a3a15-b990-4e02-abc7-78f3086a9137', 3, N'e56815f7-55e9-45d1-99ee-82926fa7c375', NULL)
 