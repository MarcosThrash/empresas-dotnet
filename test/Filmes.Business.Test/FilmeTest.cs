﻿using Filmes.Business.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Xunit;

namespace Filmes.Business.Test
{
    public class FilmeTest
    {
        [Fact(DisplayName ="Filme Valido")]
        [Trait("Categoria", "Filme")]
        public void Filme_Valido_CamposPrinicipaisPreenchidos()
        {
            //Arrange
            var filme = new Filme
            {
                Nome = "O Cavaleiro das Trevas",
                Sinopse = "Na trama, o Coringa assume o crime organizado de Gotham e representa " +
                "um dos maiores desafios que Bruce Wayne já precisou enfrentar. Sádico e violento, " +
                "ele conta diferentes histórias de origem para sua identidade criminosa, enquanto " +
                "mutila os rostos de suas vítimas.",
                GeneroId = Guid.NewGuid(),
                DiretorId = Guid.NewGuid(),
            };

            //Act
            var resultado = filme.Valido();

            //Assert
            Assert.True(resultado.IsValid);

        }

        [Fact(DisplayName = "Filme Invalido")]
        [Trait("Categoria", "Filme")]
        public void Filme_Invalido_CamposPrinicipaisPreenchidos()
        {
            //Arrange
            var filme = new Filme
            {
                Nome = "",
                Sinopse = "",
                GeneroId = new Guid(),
                DiretorId = new Guid(),
            };

            //Act
            var resultado = filme.Valido();

            //Assert
            Assert.False(resultado.IsValid);
            Assert.Equal(4, resultado.Errors.Count);

        }

        [Fact(DisplayName = "Filme Número de Votos Correto")]
        [Trait("Categoria", "Filme")]
        public void Filme_Numero_de_votos_CamposPrinicipaisPreenchidos()
        {
            //Arrange
            var filme = new Filme
            {
                Votos = new List<Voto>()
                {
                    new Voto(), new Voto(), new Voto(), new Voto()
                }
            };

            //Act
            var resultado = filme.NumeroDeVotos();

            //Assert
            Assert.Equal(4, resultado);

        }


        [Fact(DisplayName = "Filme Média de Votos Correto")]
        [Trait("Categoria", "Filme")]
        public void Filme_Média_de_votos_CamposPrinicipaisPreenchidos()
        {
            //Arrange
            var filme = new Filme
            {
                Votos = new List<Voto>()
                {
                    new Voto(){Nota = 3.5},
                    new Voto(){Nota = 4},
                    new Voto(){Nota = 2},
                    new Voto(){Nota = 1}
                }
            };

            //Act
            var resultado = filme.MediaVotos();

            //Assert
            Assert.Equal(2.625, resultado);

        }

    }
}
